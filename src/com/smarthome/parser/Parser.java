package com.smarthome.parser;

import com.smarthome.Main;
import com.smarthome.scanner.Token;
import com.smarthome.scanner.TokenType;

import java.util.ArrayList;
import java.util.List;

public class Parser {
    private final List<Token> tokens;
    private int current = 0;
    private int currentLevel = 0;

    public Parser(List<Token> tokens) {
        this.tokens = tokens;
    }

    public List<Statement> parse() {
        List<Statement> statements = new ArrayList<>();

        // read imports first
        while (!isEOF() && conform(TokenType.IMPORT)) {
            statements.add(importing());
        }

        // read program
        while (!isEOF()) {
            statements.add(declaration());
        }

        return statements;
    }

    private Expression expression() {
        return assignment();
    }


    private Statement.Import importing() {
        try {
            Token type = expect(TokenType.IDENTIFIER, "Expect importing type.");
            Token category = expect(TokenType.IDENTIFIER, "Expect importing class.");
            expect(TokenType.INDENT, "Expect indent after import statement.");

            return new Statement.Import(type, category);
        } catch (ParseError error) {
            sync();
            return null;
        }
    }

    private Statement declaration() {
        try {
            if (conform(TokenType.INDENT)) return null;
            if (conform(TokenType.COMPOUND_T)) return compoundType();
            if (conform(TokenType.DEF)) return function();
            if (conform(TokenType.WHEN)) return when();
            if (conform(TokenType.SIMPLE_T))
                return varDeclarationAndDefinition();

            return statement();
        } catch (ParseError error) {
            sync();
            return null;
        }
    }

    private Statement compoundType() {
        if (previous().getLexeme().equals("Place"))
            return placeDeclaration();
        else return thingDeclaration();
    }

    private Statement thingDeclaration() {
        Token type = previous();
        Token name = expect(TokenType.IDENTIFIER, "Expect Thing name.");

        expect(TokenType.IS, "Expect 'is' after Thing name.");
        expect(TokenType.IDENTIFIER, "Expect superPlace name.");
        Expression.Variable superThing = new Expression.Variable(previous());

        expect(TokenType.LEFT_PAREN, "Expect '(' after superPlace.");
        Expression call = finishCall(superThing);

        expect(TokenType.INDENT, "Expect indent after Thing definition.");

        return new Statement.Thing(type, name, superThing, call);
    }

    private Statement placeDeclaration() {
        Token type = previous();
        Token name = expect(TokenType.IDENTIFIER, "Expect Place name.");

        Expression.Variable superPlace = null;
        if (conform(TokenType.AT)) {
            expect(TokenType.IDENTIFIER, "Expect superPlace name.");
            superPlace = new Expression.Variable(previous());
        }

        List<Statement> body = new ArrayList<>();

        if (conform(TokenType.COLON)) {
            body = block();
        } else {
            expect(TokenType.INDENT, "Expect indent after place definition.");
        }

        return new Statement.Place(type, name, superPlace, body);
    }

    private Statement statement() {
        if (conform(TokenType.FOR)) return forStatement();
        if (conform(TokenType.IF)) return ifStatement();
        if (conform(TokenType.RETURN)) return returnStatement();
        if (conform(TokenType.PRINT)) return printStatement();

        return expressionStatement();
    }

    private Statement printStatement() {
        Token token = previous();
        Expression value = expression();
        expect(TokenType.INDENT, "Expect indent after print statement.");
        return new Statement.Print(token, value);
    }

    private Statement ifStatement() {
        Expression condition = expression();
        Token colon = expect(TokenType.COLON, "Expect ':' after if condition.");

        Statement thenBranch = new Statement.Block(block());
        Statement elseBranch = null;
        if (conform(TokenType.ELSE)) {
            expect(TokenType.COLON, "Expect ':' after else condition.");
            elseBranch = new Statement.Block(block());
        }

        return new Statement.If(condition, thenBranch, elseBranch, colon);
    }

    private Statement returnStatement() {
        Token keyword = previous();
        Expression value = null;
        if (!confirm(TokenType.INDENT)) {
            value = expression();
        }

        expect(TokenType.INDENT, "Expect indent after return value.");
        return new Statement.Return(keyword, value);
    }

    private Statement.SimpleVar varDeclaration() {
        Token type = previous();
        Token name = expect(TokenType.IDENTIFIER, "Expect variable name.");

        return new Statement.SimpleVar(type, name, null);
    }

    private Statement varDeclarationAndDefinition() {
        Token type = previous();
        Token name = expect(TokenType.IDENTIFIER, "Expect variable name.");

        expect(TokenType.EQUAL, "Expect variable value declaration.");

        Expression initializer = expression();

        expect(TokenType.INDENT, "Expect indent after variable declaration.");

        return new Statement.SimpleVar(type, name, initializer);
    }

    private Statement forStatement() {
        Token each = expect(TokenType.IDENTIFIER, "Expect identifier after 'for'.");
        expect(TokenType.IN, "Expect 'in' after each part.");
        Expression in = expression();
        expect(TokenType.COLON, "Expect ':' at the end of for statement.");

        Statement body = new Statement.Block(block());

        return new Statement.For(each, in, body);
    }

    private Statement expressionStatement() {
        Expression expression = expression();
        expect(TokenType.INDENT, "Expect indent after expression.");
        return new Statement.Expression(expression);
    }

    private Statement.Function function() {
        Token name = expect(TokenType.IDENTIFIER, "Expect function name.");
        expect(TokenType.LEFT_PAREN, "Expect '(' after function name.");
        List<Statement.SimpleVar> parameters = new ArrayList<>();
        if (!confirm(TokenType.RIGHT_PAREN)) {
            do {
                if (parameters.size() >= 64) {
                    error(peek(), "Cannot have more than 64 parameters.");
                }
                if (conform(TokenType.SIMPLE_T))
                    parameters.add(varDeclaration());
                else throw error(peek(), "Expect parameter type.");
            } while (conform(TokenType.COMMA));
        }
        expect(TokenType.RIGHT_PAREN, "Expect ')' after parameters.");

        Token returns = null;
        if (conform(TokenType.ARROW)) {
            returns = expect(TokenType.SIMPLE_T, "Expect return type.");
        }

        expect(TokenType.COLON, "Expect ':' before function body.");
        List<Statement> body = block();
        return new Statement.Function(name, parameters, returns, body);
    }

    private Statement.When when() {
        Token whenToken = previous();

        Token selector = null;
        if (conform(TokenType.ANY, TokenType.EVERY)) {
            selector = previous();
        }

        Expression expression = expression();

        boolean changed = false;
        Expression.Literal from = null;
        Expression.Literal to = null;
        if (conform(TokenType.CHANGED)) {
            changed = true;
        }

        Token fromToken = null;
        if (conform(TokenType.FROM)) {
            fromToken = previous();
            if (!changed) error(fromToken, "Expect 'changed' before 'from'.");
            from = literal();
        }

        Token toToken = null;
        if (conform(TokenType.TO)) {
            toToken = previous();
            if (!changed) error(toToken, "Expect 'changed' before 'to'.");
            to = literal();
        }

        expect(TokenType.COLON, "Expect ':' before when body.");
        List<Statement> body = block();

        return new Statement.When(selector, expression, changed, from, to, body, whenToken, fromToken, toToken);
    }

    private List<Statement> block() {
        List<Statement> statements = new ArrayList<>();

        int blockLevel = currentLevel;
        expect(TokenType.INDENT, "Expect indentation at the beginning of block.");
        while (currentLevel >= blockLevel + 1 && !isEOF()) {
            if (currentLevel != blockLevel + 1) error(previous(), "Too many indents.");
            statements.add(declaration());
        }

        return statements;
    }

    private Expression assignment() {
        Expression expression = or();

        if (conform(TokenType.EQUAL)) {
            Token equals = previous();
            Expression value = assignment();

            if (expression instanceof Expression.Variable) {
                Token name = ((Expression.Variable) expression).name;
                return new Expression.Assign(name, value);
            } else if (expression instanceof Expression.Get) {
                Expression.Get get = (Expression.Get) expression;
                return new Expression.Set(get.object, get.name, value);
            }

            error(equals, "Invalid assignment target.");
        }

        return expression;
    }

    private Expression or() {
        Expression expression = and();

        while (conform(TokenType.OR)) {
            Token operator = previous();
            Expression right = and();
            expression = new Expression.Logical(expression, operator, right);
        }

        return expression;
    }

    private Expression and() {
        Expression expression = equality();

        while (conform(TokenType.AND)) {
            Token operator = previous();
            Expression right = equality();
            expression = new Expression.Logical(expression, operator, right);
        }

        return expression;
    }

    private Expression equality() {
        Expression expression = comparison();

        while (conform(TokenType.EXCLAMATION_EQUAL, TokenType.EQUAL_EQUAL)) {
            Token operator = previous();
            Expression right = comparison();
            expression = new Expression.Binary(expression, operator, right);
        }

        return expression;
    }

    private Expression comparison() {
        Expression expression = addition();

        while (conform(TokenType.GREATER, TokenType.GREATER_EQUAL, TokenType.LESS, TokenType.LESS_EQUAL)) {
            Token operator = previous();
            Expression right = addition();
            expression = new Expression.Binary(expression, operator, right);
        }

        return expression;
    }

    private Expression addition() {
        Expression expression = multiplication();

        while (conform(TokenType.MINUS, TokenType.PLUS)) {
            Token operator = previous();
            Expression right = multiplication();
            expression = new Expression.Binary(expression, operator, right);
        }

        return expression;
    }

    private Expression multiplication() {
        Expression expression = unary();

        while (conform(TokenType.SLASH, TokenType.STAR)) {
            Token operator = previous();
            Expression right = unary();
            expression = new Expression.Binary(expression, operator, right);
        }

        return expression;
    }

    private Expression unary() {
        if (conform(TokenType.PLUS, TokenType.MINUS, TokenType.NOT)) {
            Token operator = previous();
            Expression right = unary();
            return new Expression.Unary(operator, right);
        }

        return call();
    }

    private Expression finishCall(Expression callee) {
        List<Expression> arguments = new ArrayList<>();
        boolean keywords = false;
        if (!confirm(TokenType.RIGHT_PAREN)) {
            do {
                if (arguments.size() >= 64) error(peek(), "Cannot have more than 64 arguments.");

                Expression expression = expression();
                if (!keywords) {
                    if (expression instanceof Expression.Assign) {
                        keywords = true;
                    }
                    arguments.add(expression);
                } else if (expression instanceof Expression.Assign) {
                    arguments.add(expression);
                } else {
                    error(previous(), "You can't use argument after keywords.");
                }
            } while (conform(TokenType.COMMA));
        }

        Token paren = expect(TokenType.RIGHT_PAREN, "Expect ')' after arguments.");

        return new Expression.Call(callee, paren, arguments);
    }

    private Expression call() {
        Expression expression = primary();

        while (true) {
            if (conform(TokenType.LEFT_PAREN)) {
                expression = finishCall(expression);
            } else if (conform(TokenType.DOT)) {
                Token name = expect(TokenType.IDENTIFIER, "Expect property name after '.'.");
                expression = new Expression.Get(expression, name);
            } else if (conform(TokenType.LEFT_BRACKET)) {
                Expression inside = expression();
                expect(TokenType.RIGHT_BRACKET, "Expect ']' at the end of subscription.");
                expression = new Expression.Subscription(expression, inside, previous());
            } else {
                break;
            }
        }

        return expression;
    }

    private Expression primary() {
        if (conform(TokenType.NUMBER, TokenType.STRING)) {
            return new Expression.Literal(previous().getLiteral());
        }

        if (conform(TokenType.FALSE)) return new Expression.Literal(false);
        if (conform(TokenType.TRUE)) return new Expression.Literal(true);


        if (conform(TokenType.IDENTIFIER)) {
            Token name = previous();

            if (conform(TokenType.AT_SIGN)) {
                Expression expression = call();
                return new Expression.PropertyRef(name, expression);
            }

            return new Expression.Variable(name);
        }

        if (conform(TokenType.LEFT_PAREN)) {
            Expression expression = expression();
            expect(TokenType.RIGHT_PAREN, "Expect ')' after expression.");
            return new Expression.Grouping(expression);
        }

        if (conform(TokenType.IMPORT)) {
            throw error(peek(), "Importing can be performed only at the beginning of file.");
        }

        throw error(peek(), "Expect expression.");
    }

    private Expression.Literal literal() {
        if (conform(TokenType.FALSE)) return new Expression.Literal(false);
        if (conform(TokenType.TRUE)) return new Expression.Literal(true);

        if (conform(TokenType.NUMBER, TokenType.STRING)) {
            return new Expression.Literal(previous().getLiteral());
        }

        throw error(peek(), "Expect literal.");
    }

    private boolean conform(TokenType... types) {
        for (TokenType type : types) {
            if (confirm(type)) {
                advance();
                return true;
            }
        }

        return false;
    }

    private Token expect(TokenType type, String message) {
        if (confirm(type)) {
            if (type == TokenType.INDENT) {
                currentLevel = (int) peek().getLiteral();
            }
            return advance();
        }

        throw error(peek(), message);
    }

    private boolean confirm(TokenType type) {
        if (isEOF()) return false;
        return peek().getType() == type;
    }

    private Token advance() {
        if (!isEOF()) current++;
        return previous();
    }

    private boolean isEOF() {
        return peek().getType() == TokenType.EOF;
    }

    private Token peek() {
        return tokens.get(current);
    }

    private Token previous() {
        return tokens.get(current - 1);
    }

    private ParseError error(Token token, String message) {
        Main.error(message, token.getLine(), token.getColumn());
        return new ParseError();
    }

    private void sync() {
        advance();

        while (!isEOF()) {
            if (previous().getType() == TokenType.INDENT) return;

            switch (peek().getType()) {
                case SIMPLE_T:
                case COMPOUND_T:
                case DEF:
                case WHEN:
                case FOR:
                case IF:
                case RETURN:
                    return;
            }

            advance();
        }
    }

    private static class ParseError extends RuntimeException {
    }
}
