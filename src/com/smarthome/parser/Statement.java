package com.smarthome.parser;

import com.smarthome.scanner.Token;

import java.util.List;

public abstract class Statement {
    public abstract <R> R accept(Visitor<R> visitor);

    public interface Visitor<R> {
        R visitBlockStmt(Block stmt);

        R visitPlaceStmt(Place stmt);

        R visitThingStmt(Thing stmt);

        R visitExpressionStmt(Expression stmt);

        R visitFunctionStmt(Function stmt);

        R visitWhenStmt(When stmt);

        R visitIfStmt(If stmt);

        R visitReturnStmt(Return stmt);

        R visitSimpleVarStmt(SimpleVar stmt);

        R visitForStmt(For stmt);

        R visitImportStmt(Import stmt);

        R visitPrintStmt(Print stmt);
    }

    public static class Block extends Statement {
        public final List<Statement> statements;

        Block(List<Statement> statements) {
            this.statements = statements;
        }

        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitBlockStmt(this);
        }
    }

    public static class Place extends Statement {
        public final Token type;
        public final Token name;
        public final com.smarthome.parser.Expression.Variable superPlace;
        public final List<Statement> block;

        Place(Token type, Token name,
              com.smarthome.parser.Expression.Variable superPlace,
              List<Statement> block) {
            this.type = type;
            this.name = name;
            this.superPlace = superPlace;
            this.block = block;
        }

        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitPlaceStmt(this);
        }
    }

    public static class Thing extends Statement {
        public final Token type;
        public final Token name;
        public final com.smarthome.parser.Expression.Variable superInterface;
        public final com.smarthome.parser.Expression call;

        Thing(Token type, Token name,
              com.smarthome.parser.Expression.Variable superInterface,
              com.smarthome.parser.Expression call) {
            this.type = type;
            this.name = name;
            this.superInterface = superInterface;
            this.call = call;
        }

        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitThingStmt(this);
        }
    }

    public static class Expression extends Statement {
        public final com.smarthome.parser.Expression expression;

        Expression(com.smarthome.parser.Expression expression) {
            this.expression = expression;
        }

        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitExpressionStmt(this);
        }
    }

    public static class Function extends Statement {
        public final Token name;
        public final List<Statement.SimpleVar> params;
        public final Token returns;
        public final List<Statement> body;

        Function(Token name, List<Statement.SimpleVar> params, Token returns, List<Statement> body) {
            this.name = name;
            this.params = params;
            this.returns = returns;
            this.body = body;
        }

        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitFunctionStmt(this);
        }
    }

    public static class When extends Statement {
        public final Token selector;
        public final Token fromToken;
        public final Token toToken;
        public final Token whenToken;
        public final com.smarthome.parser.Expression expression;
        public final Boolean changed;
        public final com.smarthome.parser.Expression.Literal from;
        public final com.smarthome.parser.Expression.Literal to;
        public final List<Statement> body;

        When(Token selector, com.smarthome.parser.Expression expression, Boolean changed,
             com.smarthome.parser.Expression.Literal from, com.smarthome.parser.Expression.Literal to, List<Statement> body, Token whenToken, Token fromToken, Token toToken) {
            this.selector = selector;
            this.expression = expression;
            this.changed = changed;
            this.from = from;
            this.to = to;
            this.body = body;
            this.whenToken = whenToken;
            this.fromToken = fromToken;
            this.toToken = toToken;
        }

        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitWhenStmt(this);
        }
    }

    public static class If extends Statement {
        public final com.smarthome.parser.Expression condition;
        public final Statement ifBlock;
        public final Statement elseBlock;
        public final Token colon;

        If(com.smarthome.parser.Expression condition, Statement ifBlock, Statement elseBlock, Token colon) {
            this.condition = condition;
            this.ifBlock = ifBlock;
            this.elseBlock = elseBlock;
            this.colon = colon;
        }

        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitIfStmt(this);
        }
    }

    public static class Return extends Statement {
        public final Token keyword;
        public final com.smarthome.parser.Expression value;

        Return(Token keyword, com.smarthome.parser.Expression value) {
            this.keyword = keyword;
            this.value = value;
        }

        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitReturnStmt(this);
        }
    }

    public static class SimpleVar extends Statement {
        public final Token type;
        public final Token name;
        public final com.smarthome.parser.Expression initializer;

        SimpleVar(Token type, Token name, com.smarthome.parser.Expression initializer) {
            this.type = type;
            this.name = name;
            this.initializer = initializer;
        }

        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitSimpleVarStmt(this);
        }
    }

    public static class For extends Statement {
        public final Token each;
        public final com.smarthome.parser.Expression in;
        public final Statement body;

        For(Token each, com.smarthome.parser.Expression in, Statement body) {
            this.each = each;
            this.in = in;
            this.body = body;
        }

        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitForStmt(this);
        }
    }

    public static class Import extends Statement {
        public final Token type;
        public final Token category;

        Import(Token type, Token category) {
            this.type = type;
            this.category = category;
        }

        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitImportStmt(this);
        }
    }

    public static class Print extends Statement {
        public final Token print;
        public final com.smarthome.parser.Expression expression;

        Print(Token print, com.smarthome.parser.Expression expression) {
            this.print = print;
            this.expression = expression;
        }

        public <R> R accept(Visitor<R> visitor) {
            return visitor.visitPrintStmt(this);
        }
    }
}
