package com.smarthome;

import com.smarthome.interpreter.Interpreter;
import com.smarthome.interpreter.InterpretingException;
import com.smarthome.interpreter.Resolver;
import com.smarthome.parser.Parser;
import com.smarthome.parser.Statement;
import com.smarthome.scanner.CodeReader;
import com.smarthome.scanner.Scanner;
import com.smarthome.scanner.Token;
import com.smarthome.scanner.TokenType;

import java.util.LinkedList;
import java.util.List;

public class Main {
    private static int errorCount = 0;
    private static int interpretingErrorCount = 0;

    public static void error(String message, int line, int column) {
        System.err.println(String.format("That's not good... You'd better take a look at line %d column %d: %s",
                line, column, message));
        errorCount++;
    }

    public static void interpretingError(InterpretingException error) {
        error(error.getMessage(), error.token.getLine(), error.token.getColumn());
        interpretingErrorCount++;
    }

    private static void run(String path) {
        CodeReader reader = new CodeReader(path);
        Scanner scanner = new Scanner(reader);

        List<Token> tokens = new LinkedList<>();
        Token token = scanner.getToken();
        do {
            if (token.getType() == TokenType.ERROR) {
                error((String) token.getLiteral(), token.getLine(), token.getColumn());
            } else {
                tokens.add(token);
            }
            token = scanner.getToken();
        } while (token.getType() != TokenType.EOF);
        tokens.add(token);  // Add EOF token

        Parser parser = new Parser(tokens);
        List<Statement> statements = parser.parse();

        stopIfError(errorCount);

        Interpreter interpreter = new Interpreter();
        Resolver resolver = new Resolver(interpreter);
        resolver.resolve(statements);

        stopIfError(errorCount);

        interpreter.interpret(statements);

        stopIfError(interpretingErrorCount);
        congratulateIfNoError(interpretingErrorCount);

        System.out.println("System is running...");

        Thread runner = interpreter.runThread();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                runner.interrupt();
                runner.join();
            } catch (InterruptedException ignored) {
            } finally {
                System.out.println("Shutdown");
            }
        }));

        runner.run();
    }

    private static void stopIfError(int errorCount) {
        if (errorCount > 0) {
            System.err.println(String.format("Oh no! You've made %d error(s) in your code :(. " +
                    "Hope it wouldn't take long to correct them.", errorCount));
            System.exit(1);
        }
    }

    private static void congratulateIfNoError(int errorCount) {
        if (errorCount == 0) {
            System.out.println("Lucky you! Everything works just perfect.");
        }
    }

    public static void main(String[] args) {
        if (args.length == 1) {
            run(args[0]);
        } else {
            System.out.println("Usage: smh fpath");
        }
    }

    public static int getErrorCount() {
        return errorCount;
    }

    public static int getInterpretingErrorCount() {
        return interpretingErrorCount;
    }

    public static void resetErrors() {
        interpretingErrorCount = 0;
        errorCount = 0;
    }
}
