package com.smarthome.scanner;

public interface Source {
    char getChar();

    char peek(int ahead);

    boolean isEOF();

    Position getPosition();
}

