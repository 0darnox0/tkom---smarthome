package com.smarthome.scanner;

public class Token {
    private final TokenType type;
    private final String lexeme;
    private final Object literal;
    private final int column;
    private final int line;

    public Token(TokenType type, String lexeme, Object literal, int line, int column) {
        this.type = type;
        this.lexeme = lexeme;
        this.literal = literal;
        this.line = line;
        this.column = column;
    }

    public TokenType getType() {
        return type;
    }

    public String getLexeme() {
        return lexeme;
    }

    public Object getLiteral() {
        return literal;
    }

    public int getColumn() {
        return column;
    }

    public int getLine() {
        return line;
    }

    public String toString() {
        return type + ": '" + lexeme + "' => " + literal + " at (" + line + ":" + column + ")";
    }
}
