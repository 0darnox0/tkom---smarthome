package com.smarthome.scanner;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class Scanner {
    private static final int MAX_LENGTH = Integer.MAX_VALUE - 1;
    private static final Map<String, TokenType> keywords;

    static {
        keywords = new HashMap<>();

        keywords.put("import", TokenType.IMPORT);
        keywords.put("Bool", TokenType.SIMPLE_T);
        keywords.put("String", TokenType.SIMPLE_T);
        keywords.put("Number", TokenType.SIMPLE_T);
        keywords.put("Property", TokenType.SIMPLE_T);
        keywords.put("Place", TokenType.COMPOUND_T);
        keywords.put("Thing", TokenType.COMPOUND_T);
        keywords.put("is", TokenType.IS);
        keywords.put("def", TokenType.DEF);
        keywords.put("at", TokenType.AT);
        keywords.put("for", TokenType.FOR);
        keywords.put("in", TokenType.IN);
        keywords.put("when", TokenType.WHEN);
        keywords.put("any", TokenType.ANY);
        keywords.put("every", TokenType.EVERY);
        keywords.put("changed", TokenType.CHANGED);
        keywords.put("from", TokenType.FROM);
        keywords.put("to", TokenType.TO);
        keywords.put("False", TokenType.FALSE);
        keywords.put("True", TokenType.TRUE);
        keywords.put("if", TokenType.IF);
        keywords.put("else", TokenType.ELSE);
        keywords.put("not", TokenType.NOT);
        keywords.put("and", TokenType.AND);
        keywords.put("or", TokenType.OR);
        keywords.put("return", TokenType.RETURN);
        keywords.put("print", TokenType.PRINT);
    }

    private final Map<Character, Supplier<Token>> functions;
    private final Source source;
    private Token lastToken = null;
    private StringBuilder lexeme;
    private Boolean EOF = false;

    {
        functions = new HashMap<>();

        functions.put('(', () -> setToken(TokenType.LEFT_PAREN));
        functions.put(')', () -> setToken(TokenType.RIGHT_PAREN));
        functions.put('[', () -> setToken(TokenType.LEFT_BRACKET));
        functions.put(']', () -> setToken(TokenType.RIGHT_BRACKET));
        functions.put(':', () -> setToken(TokenType.COLON));
        functions.put(',', () -> setToken(TokenType.COMMA));
        functions.put('.', () -> setToken(TokenType.DOT));
        functions.put('@', () -> setToken(TokenType.AT_SIGN));
        functions.put('-', () -> setToken(expect('>') ? TokenType.ARROW : TokenType.MINUS));
        functions.put('+', () -> setToken(TokenType.PLUS));
        functions.put('*', () -> setToken(TokenType.STAR));
        functions.put('!', () -> setToken(expect('=') ? TokenType.EXCLAMATION_EQUAL : TokenType.EXCLAMATION));
        functions.put('=', () -> setToken(expect('=') ? TokenType.EQUAL_EQUAL : TokenType.EQUAL));
        functions.put('<', () -> setToken(expect('=') ? TokenType.LESS_EQUAL : TokenType.LESS));
        functions.put('>', () -> setToken(expect('=') ? TokenType.GREATER_EQUAL : TokenType.GREATER));
        functions.put('/', () -> setToken(TokenType.SLASH));
        functions.put('"', this::string);
        functions.put('\n', this::indent);
        // Set numbers
        for (char c = '0'; c <= '9'; c++) {
            functions.put(c, this::number);
        }
        // Set lowercase letters
        for (char c = 'a'; c <= 'z'; c++) {
            functions.put(c, this::identifier);
        }
        // Set uppercase letters
        for (char c = 'A'; c <= 'Z'; c++) {
            functions.put(c, this::identifier);
        }
    }

    public Scanner(Source source) {
        this.source = source;
        this.lexeme = new StringBuilder();
    }

    private boolean isEOF() {
        return EOF;
    }

    public Token getToken() {
        skipUnrelevant();

        char c = advance();

        if (isEOF()) {
            return new Token(TokenType.EOF, "", null, line(), column());
        } else if (c == (char) -1 || c == (char) 0x04) {  // 0x04 == EOT
            EOF = true;
            if (lastToken == null || lastToken.getType() != TokenType.INDENT)
                return new Token(TokenType.INDENT, "", 0, line(), column());

            return new Token(TokenType.EOF, "", null, line(), column());
        }

        Token token = functions.getOrDefault(c, () -> unexpCharError(c)).get();
        lastToken = token;

        if (token == null) return getToken();
        return token;
    }

    private void skipUnrelevant() {
        char c = peek();
        while (skipWhitespace(c) || skipComment(c)) {
            getLexeme();
            c = peek();
        }
    }

    private boolean skipWhitespace(char c) {
        if (isWhitespace(c)) {
            advance();
            return true;
        }
        return false;
    }

    private boolean skipComment(char c) {
        if (c == '#') {
            while (peek() != '\n' && !isEOF()) advance();
            return true;
        }
        return false;
    }

    private Token identifier() {
        int length = 0;
        while (isAlphaNumeric(peek())) {
            advance();
            if (length++ == MAX_LENGTH) return lengthError();
        }

        String text = previewLexeme();

        TokenType type = keywords.get(text);
        if (type == null) type = TokenType.IDENTIFIER;

        return setToken(type);
    }

    private Token number() {
        int length = 0;
        while (isDigit(peek())) {
            advance();
            if (length++ == MAX_LENGTH) return lengthError();
        }

        if (peek() == '.' && isDigit(peek(1))) {
            advance();
            while (isDigit(peek())) {
                advance();
                if (length++ == MAX_LENGTH) return lengthError();
            }
        }

        return setToken(TokenType.NUMBER, Double.parseDouble(previewLexeme()));
    }

    private boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    private boolean isAlpha(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }

    private boolean isAlphaNumeric(char c) {
        return isAlpha(c) || isDigit(c) || c == '_';
    }

    private boolean isWhitespace(char c) {
        return c == ' ' || c == '\t' || c == '\r';
    }

    private Token string() {
        int length = 0;
        while (peek() != '"') {
            if (advance() == '\n' || source.isEOF()) {
                return stringError();
            }
            if (length++ == MAX_LENGTH) return lengthError();
        }
        advance();

        String value = previewLexeme();
        value = value.substring(1, value.length() - 1); // Remove "" marks
        return setToken(TokenType.STRING, value);
    }

    private Token indent() {
        int length = 0;
        while (peek() == ' ' && !isEOF()) {
            advance();
            if (length++ == MAX_LENGTH) return lengthError();
        }

        if (peek() == '\r') {
            return null;
        }

        if (length % 4 != 0) return indentError(length);

        int level = length / 4;
        return setToken(TokenType.INDENT, level);
    }

    private char peek() {
        return peek(0);
    }

    private char peek(int ahead) {
        return source.peek(ahead);
    }

    private boolean expect(int expected) {
        if (peek() == expected) {
            advance();
            return true;
        }
        return false;
    }

    private char advance() {
        char c = source.getChar();
        lexeme.append(c);

        return c;
    }

    private Token setToken(TokenType type) {
        return setToken(type, null);
    }

    private String previewLexeme() {
        return lexeme.toString();
    }

    private String getLexeme() {
        String text = lexeme.toString();
        lexeme.delete(0, lexeme.length());

        return text;
    }

    private int column() {
        return source.getPosition().column();
    }

    private int line() {
        return source.getPosition().line();
    }

    private Token setToken(TokenType type, Object literal) {
        String text = getLexeme();
        int line = line();
        int start_column = column() - text.length() + 1;

        return new Token(type, text, literal, line, start_column);
    }

    private Token lengthError() {
        return setToken(TokenType.ERROR, "You made it way too long.");
    }

    private Token stringError() {
        return setToken(TokenType.ERROR, "It looks like you've forgotten about closing the string.");
    }

    private Token unexpCharError(char c) {
        return setToken(TokenType.ERROR, String.format("'%s' - this guy shouldn't be there.", c));
    }

    private Token indentError(int l) {
        return setToken(TokenType.ERROR, String.format("I'm sure that %d spaces aren't divisible by 4...", l));
    }
}