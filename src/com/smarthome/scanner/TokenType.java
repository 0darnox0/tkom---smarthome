package com.smarthome.scanner;

public enum TokenType {
    // Single-character tokens.
    LEFT_PAREN,        // (
    RIGHT_PAREN,       // (
    LEFT_BRACKET,      // [
    RIGHT_BRACKET,     // ]
    COMMA,             // ,
    DOT,               // .
    MINUS,             // -
    PLUS,              // +
    SLASH,             // /
    STAR,              // *
    AT_SIGN,           // @
    COLON,             // :

    // One or two character tokens.
    EXCLAMATION,       // !
    EXCLAMATION_EQUAL, // !=
    EQUAL,             // =
    EQUAL_EQUAL,       // ==
    GREATER,           // >
    GREATER_EQUAL,     // >=
    LESS,              // <
    LESS_EQUAL,        // <=
    ARROW,             // ->

    // Literals.
    IDENTIFIER,        // e.g. window
    STRING,            // e.g. "Long sentence"
    NUMBER,            // e.g. 1.3
    INDENT,            // '\n    '

    // Keywords.
    IMPORT,            // import
    SIMPLE_T,          // Bool, Number, String, Property
    COMPOUND_T,        // Place, Thing
    IS,                // is
    AT,                // at
    DEF,               // def
    RETURN,            // return
    FOR,               // for
    IN,                // in
    IF,                // if
    ELSE,              // else
    NOT,               // not
    AND,               // and
    OR,                // or
    TRUE,              // true
    FALSE,             // false
    WHEN,              // when
    EVERY,             // every
    ANY,               // any
    CHANGED,           // changed
    FROM,              // from
    TO,                // to
    PRINT,             // print

    // End of file
    EOF,

    // Error
    ERROR,
}
