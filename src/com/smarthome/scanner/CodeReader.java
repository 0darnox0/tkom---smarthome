package com.smarthome.scanner;

import java.io.*;

public class CodeReader implements Source {
    private final Position currentPos; // Position of recently read char
    private final StringBuilder readBuffer;
    private File file;
    private BufferedReader reader;
    private boolean EOF = false;
    private char c;  // Last char

    public CodeReader(Reader reader) {
        currentPos = new Position(1, 0);
        this.reader = new BufferedReader(reader);
        this.readBuffer = new StringBuilder();
    }

    public CodeReader(String fileName) {
        currentPos = new Position(1, 0);
        try {
            file = new File(fileName);
            reader = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        this.readBuffer = new StringBuilder();
    }

    @Override
    public boolean isEOF() {
        return EOF;
    }

    @Override
    public Position getPosition() {
        return currentPos;
    }

    @Override
    public char peek(int ahead) {
        int extend = ahead - readBuffer.length();
        try {
            while (extend >= 0) {
                readBuffer.append((char) reader.read());
                extend--;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return (char) -1;
        }

        return readBuffer.charAt(ahead);
    }

    @Override
    public char getChar() {
        // Check previous char and alter position
        if (c == '\n') {
            currentPos.setColumn(0);
            currentPos.incrementLine();
        }
        currentPos.incrementColumn();

        // Get new char
        c = peek(0);
        readBuffer.deleteCharAt(0);

        if (c == (char) -1) EOF = true;

        return c;
    }
}
