package com.smarthome.test;

import com.smarthome.Main;
import com.smarthome.interpreter.Interpreter;
import com.smarthome.interpreter.Resolver;
import com.smarthome.parser.Parser;
import com.smarthome.parser.Statement;
import com.smarthome.scanner.CodeReader;
import com.smarthome.scanner.Scanner;
import com.smarthome.scanner.Token;
import com.smarthome.scanner.TokenType;
import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestCases {
    static String removeNewLine(String text) {
        return text.replaceAll("\\r?\\n", "\n");
    }

    @Test
    void testCase1() {
        String program = "Bool a = True\n" +
                "Number b = 5\n" +
                "String c = \"c\"\n" +
                "print a\n" +
                "print b\n" +
                "print c";

        String expectedOutput = "true\n" +
                "5.0\n" +
                "c";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase2() {
        String program = "Bool a\n" +
                "Number b\n" +
                "String c\n";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 7: Expect variable value declaration.\n" +
                "That's not good... You'd better take a look at line 2 column 9: Expect variable value declaration.\n" +
                "That's not good... You'd better take a look at line 3 column 9: Expect variable value declaration.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase3() {
        String program = "Bool a = 1\n" +
                "Number b = \"x\"\n" +
                "String d = True";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 6: Variable initializer is of different type than variable.\n" +
                "That's not good... You'd better take a look at line 2 column 8: Variable initializer is of different type than variable.\n" +
                "That's not good... You'd better take a look at line 3 column 8: Variable initializer is of different type than variable.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase4() {
        String program = "if True:\n" +
                "    print \"true\"\n" +
                "else:\n" +
                "    print \"false\"\n" +
                "\n" +
                "if not True:\n" +
                "    print \"true\"\n" +
                "else:\n" +
                "    print \"false\"";

        String expectedOutput = "true\n" +
                "false";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase5() {
        String program = "String a = \"a\"\n" +
                "Number a = 5";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 2 column 8: Item with this name already declared in this scope.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase6() {
        String program = "String a = \"a\n";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 12: It looks like you've forgotten about closing the string.\n" +
                "That's not good... You'd better take a look at line 2 column 1: Expect expression.";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase7() {
        String program = "Number a = .5\n" +
                "print a";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 12: Expect expression.";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase8() {
        String program = "Number a = 0.5\n" +
                "print a";

        String expectedOutput = "0.5";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase9() {
        String program = "Number a = 5.\n" +
                "print a";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 14: Expect property name after '.'.";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase10() {
        String program = "Number a = a\n" +
                "String b = b\n" +
                "Bool c = c";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 12: Cannot read local variable in its own initializer.\n" +
                "That's not good... You'd better take a look at line 2 column 12: Cannot read local variable in its own initializer.\n" +
                "That's not good... You'd better take a look at line 3 column 10: Cannot read local variable in its own initializer.";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase11() {
        String program = "Bool a = True\n" +
                "\n" +
                "if a:\n" +
                "    print \"true\"\n" +
                "\n" +
                "if not a:\n" +
                "    print \"false\"";

        String expectedOutput = "true";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase12() {
        String program = "Number a = 1\n" +
                "\n" +
                "if a:\n" +
                "    print \"ture\"\n" +
                "else:\n" +
                "    print \"false\"";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 3 column 5: Condition must be of Bool type.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase13() {
        String program = "Number a = 5\n" +
                "String b = \"b\"\n" +
                "Bool c = True\n" +
                "\n" +
                "Number d = a\n" +
                "String e = b\n" +
                "Bool f = c\n" +
                "\n" +
                "print d\n" +
                "print e\n" +
                "print f";

        String expectedOutput = "5.0\n" +
                "b\n" +
                "true";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase14() {
        String program = "Number a = 5\n" +
                "String b = \"b\"\n" +
                "Bool c = True\n" +
                "\n" +
                "Number d = c\n" +
                "String e = a\n" +
                "Bool f = a\n" +
                "\n" +
                "print d\n" +
                "print c\n" +
                "print d";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 5 column 8: Variable initializer is of different type than variable.\n" +
                "That's not good... You'd better take a look at line 6 column 8: Variable initializer is of different type than variable.\n" +
                "That's not good... You'd better take a look at line 7 column 6: Variable initializer is of different type than variable.";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase15() {
        String program = "import interface Device";

        String expectedOutput = "";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase16() {
        String program = "import nothing Device";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 8: Unrecognized importing type. Allowed are: 'interface'.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase17() {
        String program = "import interface";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 17: Expect importing class.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase18() {
        String program = "import interface Device\n" +
                "\n" +
                "Number a = 5\n" +
                "\n" +
                "import interface Window";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 5 column 8: Importing can be performed only at the beginning of file.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase19() {
        String program = "import interface Device\n" +
                "import interface Device";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 2 column 18: Item with this name already declared in this scope.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase20() {
        String program = "import";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 7: Expect importing type.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase21() {
        String program = "Place JohnsRoom\n" +
                "Place";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 2 column 6: Expect Place name.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase22() {
        String program = "import interface Device\n" +
                "Thing window is Device(arg=data,arg2=data2)\n" +
                "Thing\n" +
                "Thing window2\n" +
                "Thing window3 Device(arg=data,arg2=data2)\n" +
                "Thing window4 is Device\n" +
                "Thing window5 is (arg=data,arg2=data2)";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 3 column 6: Expect Thing name.\n" +
                "That's not good... You'd better take a look at line 4 column 14: Expect 'is' after Thing name.\n" +
                "That's not good... You'd better take a look at line 5 column 15: Expect 'is' after Thing name.\n" +
                "That's not good... You'd better take a look at line 6 column 24: Expect '(' after superPlace.\n" +
                "That's not good... You'd better take a look at line 7 column 18: Expect superPlace name.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase23() {
        String program = "import interface Device\n" +
                "Thing window is Window()";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 2 column 17: Cannot resolve symbol 'Window'.\n" +
                "That's not good... You'd better take a look at line 2 column 7: SuperInterface must be an Interface.\n" +
                "That's not good... You'd better take a look at line 2 column 17: Cannot resolve symbol 'Window'.\n" +
                "That's not good... You'd better take a look at line 2 column 24: This is not callable.";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase24() {
        String program = "\n" +
                "#Nothing hill\n" +
                "Number a = 5 #Nothing hill\n" +
                "#Nothing hill\n" +
                "#Nothing hill\n" +
                "####Nothing hill\n" +
                "Bool b = True    #Nothing hill\n" +
                "#Nothing hill #Nothing hill #Nothing hill #Nothing hill\n" +
                "\n" +
                "print a\n" +
                "print b\n" +
                "\n" +
                "String c = #Nothing hill \"test\"\n";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 13 column 32: Expect expression.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase25() {
        String program = "Bool g = 5 > 2\n" +
                "Bool ge = 5 >= 5\n" +
                "Bool ge2 = 6 >= 5\n" +
                "Bool l = 2 < 5\n" +
                "Bool le = 5 <= 5\n" +
                "Bool le2 = 5 <= 6\n" +
                "Bool e = 5 == 5\n" +
                "Bool ne = 5 != 2\n" +
                "\n" +
                "print g\n" +
                "print ge\n" +
                "print ge2\n" +
                "print l\n" +
                "print le\n" +
                "print le2\n" +
                "print e\n" +
                "print ne";

        String expectedOutput = "true\n" +
                "true\n" +
                "true\n" +
                "true\n" +
                "true\n" +
                "true\n" +
                "true\n" +
                "true";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase26() {
        String program = "Bool g = 5 < 2\n" +
                "Bool ge = 5 >= 6\n" +
                "Bool l = 5 < 2\n" +
                "Bool le = 6 <= 5\n" +
                "Bool e = 2 == 5\n" +
                "Bool ne = 5 != 5\n" +
                "\n" +
                "print g\n" +
                "print ge\n" +
                "print l\n" +
                "print le\n" +
                "print e\n" +
                "print ne";

        String expectedOutput = "false\n" +
                "false\n" +
                "false\n" +
                "false\n" +
                "false\n" +
                "false";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase27() {
        String program = "print True or False\n" +
                "print False or True\n" +
                "print False and True\n" +
                "print True and False";

        String expectedOutput = "true\n" +
                "true\n" +
                "false\n" +
                "false";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase28() {
        String program = "Number a = 5\n" +
                "Number b = 19\n" +
                "\n" +
                "print a + b\n" +
                "print a - b\n" +
                "print a / b\n" +
                "print a * b";

        String expectedOutput = "24.0\n" +
                "-14.0\n" +
                "0.2631578947368421\n" +
                "95.0";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase29() {
        String program = "Number a = 5\n" +
                "Number b = 19\n" +
                "\n" +
                "print a < b\n" +
                "print a <= b\n" +
                "print a > b\n" +
                "print a >= b";

        String expectedOutput = "true\n" +
                "true\n" +
                "false\n" +
                "false";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase30() {
        String program = "String a = \"5\"\n" +
                "String b = \"19\"\n" +
                "\n" +
                "print a + b\n" +
                "print a - b\n" +
                "print a / b\n" +
                "print a * b\n" +
                "print a < b\n" +
                "print a <= b\n" +
                "print a > b\n" +
                "print a >= b";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 5 column 9: Operands must be numbers.\n" +
                "That's not good... You'd better take a look at line 6 column 9: Operands must be numbers.\n" +
                "That's not good... You'd better take a look at line 7 column 9: Operands must be numbers.\n" +
                "That's not good... You'd better take a look at line 8 column 9: Operands must be numbers.\n" +
                "That's not good... You'd better take a look at line 9 column 9: Operands must be numbers.\n" +
                "That's not good... You'd better take a look at line 10 column 9: Operands must be numbers.\n" +
                "That's not good... You'd better take a look at line 11 column 9: Operands must be numbers.";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase31() {
        String program = "Bool a = True\n" +
                "Bool b = False\n" +
                "\n" +
                "print a + b\n" +
                "print a - b\n" +
                "print a / b\n" +
                "print a * b\n" +
                "print a < b\n" +
                "print a <= b\n" +
                "print a > b\n" +
                "print a >= b";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 4 column 9: Operands must be two numbers or two strings.\n" +
                "That's not good... You'd better take a look at line 5 column 9: Operands must be numbers.\n" +
                "That's not good... You'd better take a look at line 6 column 9: Operands must be numbers.\n" +
                "That's not good... You'd better take a look at line 7 column 9: Operands must be numbers.\n" +
                "That's not good... You'd better take a look at line 8 column 9: Operands must be numbers.\n" +
                "That's not good... You'd better take a look at line 9 column 9: Operands must be numbers.\n" +
                "That's not good... You'd better take a look at line 10 column 9: Operands must be numbers.\n" +
                "That's not good... You'd better take a look at line 11 column 9: Operands must be numbers.";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase32() {
        String program = "String a = \"5\"\n" +
                "String b = \"19\"\n" +
                "String c = \"19\"\n" +
                "\n" +
                "print a == b\n" +
                "print a != b\n" +
                "print c == b\n" +
                "print c != b";

        String expectedOutput = "false\n" +
                "true\n" +
                "true\n" +
                "false";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase33() {
        String program = "Number a = 5\n" +
                "Number b = 19\n" +
                "Number c = 19\n" +
                "\n" +
                "print a == b\n" +
                "print a != b\n" +
                "print c == b\n" +
                "print c != b";

        String expectedOutput = "false\n" +
                "true\n" +
                "true\n" +
                "false";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase34() {
        String program = "Bool a = True\n" +
                "Bool b = False\n" +
                "Bool c = False\n" +
                "\n" +
                "print a == b\n" +
                "print a != b\n" +
                "print c == b\n" +
                "print c != b";

        String expectedOutput = "false\n" +
                "true\n" +
                "true\n" +
                "false";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase35() {
        String program = "import interface Device\n" +
                "\n" +
                "Thing d1 is Device()\n" +
                "Thing d2 is Device()\n" +
                "Thing d3 is Device()\n" +
                "\n" +
                "for device in Device:\n" +
                "    print device";

        String expectedOutput = "d1\n" +
                "d2\n" +
                "d3";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase36() {
        String program = "import interface Device\n" +
                "\n" +
                "Thing d1 is Device()\n" +
                "Thing d2 is Device()\n" +
                "Thing d3 is Device()\n" +
                "\n" +
                "Number a = 5\n" +
                "\n" +
                "for device in a:\n" +
                "    print device";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 9 column 5: Item is not iterable.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase37() {
        String program = "import interface Device\n" +
                "\n" +
                "Thing d1 is Device()\n" +
                "\n" +
                "for device in Device:\n" +
                "    print dev";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 6 column 11: Cannot resolve symbol 'dev'.\n" +
                "That's not good... You'd better take a look at line 6 column 5: No value to print";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase38() {
        String program = "import interface Device\n" +
                "\n" +
                "Thing d1 is Device()\n" +
                "\n" +
                "for device in Device\n" +
                "    print dev";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 6 column 0: Expect ':' at the end of for statement.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase39() {
        String program = "import interface Device\n" +
                "\n" +
                "Thing d1 is Device()\n" +
                "\n" +
                "for device in Device:\n" +
                "print dev";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 5 column 5: Expect statement in a for body.\n" +
                "That's not good... You'd better take a look at line 6 column 7: Cannot resolve symbol 'dev'.\n" +
                "That's not good... You'd better take a look at line 6 column 1: No value to print";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase40() {
        String program = "import interface Device\n" +
                "\n" +
                "Thing d1 is Device()\n" +
                "\n" +
                "for device in Device:\n" +
                "     print dev";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 6 column 0: I'm sure that 5 spaces aren't divisible by 4...\n" +
                "That's not good... You'd better take a look at line 6 column 6: Expect indentation at the beginning of block.";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase41() {
        String program = "import interface Device\n" +
                "\n" +
                "Thing d1 is Device()\n" +
                "\n" +
                "for device in Device:\n" +
                "        print dev";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 6 column 0: Too many indents.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase42() {
        String program = "Number a = 5\n" +
                "a = 7\n" +
                "a = a + 3\n" +
                "print a";

        String expectedOutput = "10.0";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase43() {
        String program = "Number a = 5\n" +
                "\n" +
                "def function(Number a, Number b) -> Number:\n" +
                "    a = 7\n" +
                "    return b + 2\n" +
                "\n" +
                "function(0, 10)\n" +
                "\n" +
                "print a";

        String expectedOutput = "5.0";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase44() {
        String program = "Number a = 5\n" +
                "\n" +
                "def function(Number a, Number b) -> Number:\n" +
                "    a = 7\n" +
                "    return b + 2\n" +
                "    print a\n" +
                "\n" +
                "function(0, 10)";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 3 column 37: Last statement in function must be a return statement.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase45() {
        String program = "Number a = 5\n" +
                "\n" +
                "def function(Number a, Number b) -> Number:\n" +
                "    print a\n" +
                "    a = 7\n" +
                "    return b + 2\n" +
                "\n" +
                "function(0, 10)";

        String expectedOutput = "0.0";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase46() {
        String program = "Number a = 5\n" +
                "\n" +
                "def function(Number a, Number b) -> Number:\n" +
                "    a = 7\n" +
                "    return b + 2\n" +
                "\n" +
                "print function(0, 10)";

        String expectedOutput = "12.0";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase47() {
        String program = "def function():\n" +
                "    print \"function\"\n" +
                "\n" +
                "function()";

        String expectedOutput = "function";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase48() {
        String program = "def function(Number a):\n" +
                "    print \"function\"\n" +
                "\n" +
                "function()";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 4 column 10: Expected 1 arguments but got 0.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase49() {
        String program = "def function(Number a):\n" +
                "    print \"function\"\n" +
                "\n" +
                "function(5, 9)";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 4 column 14: Expected 1 arguments but got 2.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase50() {
        String program = "def function(Number a):\n" +
                "    print \"function\"\n" +
                "\n" +
                "function(\"5\")";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 4 column 13: Incorrect type of argument number 1.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase51() {
        String program = "def function():\n" +
                "    return\n" +
                "    print \"function\"\n" +
                "\n" +
                "function()";

        String expectedOutput = "";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase52() {
        String program = "def function():\n" +
                "    print \"function\"\n" +
                "    return\n" +
                "\n" +
                "function()";

        String expectedOutput = "function";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase53() {
        String program = "def function():\n" +
                "    print \"function\"\n" +
                "    return 5\n" +
                "\n" +
                "function()";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 3 column 5: Cannot return a value form a function with no return type declared.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase54() {
        String program = "def function() -> Number:\n" +
                "    print \"function\"\n" +
                "    return 5\n" +
                "\n" +
                "function()";

        String expectedOutput = "function";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase55() {
        String program = "def function() -> Number:\n" +
                "    print \"function\"\n" +
                "    return\n" +
                "\n" +
                "function()";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 3 column 5: Missing return value.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase56() {
        String program = "def function() -> Number:\n" +
                "    print \"function\"\n" +
                "    return \"5\"\n" +
                "\n" +
                "function()";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 3 column 5: Incorrect return type.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase57() {
        String program = "def function() -> Number:\n" +
                "    print \"function\"\n" +
                "\n" +
                "function()";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 19: Last statement in function must be a return statement.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase58() {
        String program = "def function() -> Number:\n" +
                "    return 5\n" +
                "    print \"function\"\n" +
                "\n" +
                "function()";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 19: Last statement in function must be a return statement.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase59() {
        String program = "def function() -> Number:\n" +
                "    return 5\n" +
                "\n" +
                "function";

        String expectedOutput = "";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase60() {
        String program = "def function() -> Number:\n" +
                "    return 5\n" +
                "\n" +
                "Number a = function";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 4 column 8: Variable initializer is of different type than variable.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase61() {
        String program = "import interface Window\n" +
                "Place JohnsRoom:\n" +
                "    String name = \"John's room\"\n" +
                "    Number id   = 123456\n" +
                "    Thing window is Window(id=5)\n" +
                "    def openWindow() -> Number:\n" +
                "        if id > 10:\n" +
                "            return 20\n" +
                "        else:\n" +
                "            return 5\n" +
                "        \n" +
                "        return 0\n" +
                "print JohnsRoom.name\n" +
                "print JohnsRoom.id\n" +
                "print JohnsRoom.openWindow()\n" +
                "JohnsRoom.id = 4\n" +
                "print JohnsRoom.openWindow()";

        String expectedOutput = "John's room\n" +
                "123456.0\n" +
                "20.0\n" +
                "5.0";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase62() {
        String program = "import interface Window\n" +
                "Place JohnsRoom:\n" +
                "    String name = \"John's room\"\n" +
                "    Number id   = 123456\n" +
                "    Thing window is Window(id=5)\n" +
                "    def openWindow() -> Number:\n" +
                "        if id > 10:\n" +
                "            return 20\n" +
                "        else:\n" +
                "            return 5\n" +
                "        \n" +
                "        return 0\n" +
                "print JohnsRoom.door";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 13 column 17: 'JohnsRoom' doesn't have 'door' property.\n" +
                "That's not good... You'd better take a look at line 13 column 1: No value to print";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase63() {
        String program = "Place SecondFloor:\n" +
                "    String test = \"test\"\n" +
                "Place JohnsRoom at SecondFloor:\n" +
                "    String name = \"John's room\"\n" +
                "print JohnsRoom.test";

        String expectedOutput = "test";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase64() {
        String program = "Place SecondFloor:\n" +
                "    String test = \"test\"\n" +
                "Place JohnsRoom at SecondFloor:\n" +
                "    String name = \"John's room\"\n" +
                "print SecondFloor.name";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 5 column 19: 'SecondFloor' doesn't have 'name' property.\n" +
                "That's not good... You'd better take a look at line 5 column 1: No value to print";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase65() {
        String program = "Place SecondFloor:\n" +
                "    String test = \"test\"\n" +
                "Place JohnsRoom at SecondFloor:\n" +
                "    String name = \"John's room\"\n" +
                "    String test = \"JohnsRoom test\"\n" +
                "print JohnsRoom.name";

        String expectedOutput = "John's room";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase66() {
        String program = "Place JohnsRoom at SecondFloor:\n" +
                "    String name = \"John's room\"\n" +
                "    String test = \"JohnsRoom test\"\n" +
                "Place SecondFloor:\n" +
                "    String test = \"test\"";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 20: Cannot resolve symbol 'SecondFloor'.\n" +
                "That's not good... You'd better take a look at line 1 column 7: SuperPlace must be a Place.";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase67() {
        String program = "Place JohnsRoom at JohnsRoom:\n" +
                "    String name = \"John's room\"";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 20: Cannot read local variable in its own initializer.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase68() {
        String program = "when False:\n" +
                "    print \"false\"";

        String expectedOutput = "";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase69() {
        String program = "when 5:\n" +
                "    print 5";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 1: When condition must be of Bool type.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase70() {
        String program = "when \"20\" changed form 20 to 24:\n" +
                "    print \"20\"";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 19: Expect ':' before when body.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase71() {
        String program = "when \"24\" changed to 24:\n" +
                "    print \"24\"";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 19: To value must be of the same type as condition expression.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase72() {
        String program = "when \"20\" changed from 20:\n" +
                "    print \"20\"";

        String expectedOutput = "";
        String expectedErrors = "That's not good... You'd better take a look at line 1 column 19: From value must be of the same type as condition expression.\n";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @Test
    void testCase73() {
        String program = "Number a = 5\n" +
                "\n" +
                "when a changed to 5:\n" +
                "    print a";

        String expectedOutput = "";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    private static final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private static final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private static final PrintStream originalOut = System.out;
    private static final PrintStream originalErr = System.err;

    @BeforeAll
    static void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    void testCase74() {
        String program = "";

        String expectedOutput = "";
        String expectedErrors = "";

        interpretCode(program, expectedOutput, expectedErrors);
    }

    @AfterAll
    static void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @BeforeEach
    void resetStreams() {
        outContent.reset();
        errContent.reset();
        Main.resetErrors();
    }

    void interpretCode(String program, String expectedOutput, String expectedErrors) {
        CodeReader reader = new CodeReader(new StringReader(program));
        Scanner scanner = new Scanner(reader);

        List<Token> tokens = new LinkedList<>();
        Token token = scanner.getToken();
        do {
            if (token.getType() == TokenType.ERROR) {
                Main.error((String) token.getLiteral(), token.getLine(), token.getColumn());
            } else {
                tokens.add(token);
            }
            token = scanner.getToken();
        } while (token.getType() != TokenType.EOF);
        tokens.add(token);  // Add EOF token

        Parser parser = new Parser(tokens);
        List<Statement> statements = parser.parse();

        if (Main.getErrorCount() == 0) {
            Interpreter interpreter = new Interpreter();
            Resolver resolver = new Resolver(interpreter);
            resolver.resolve(statements);

            if (Main.getErrorCount() + Main.getInterpretingErrorCount() == 0) {
                interpreter.interpret(statements);
            }
        }

        assertEquals(removeNewLine(expectedOutput.trim()), removeNewLine(outContent.toString().trim()));
        assertEquals(removeNewLine(expectedErrors.trim()), removeNewLine(errContent.toString().trim()));
    }
}
