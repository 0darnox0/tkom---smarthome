package com.smarthome.interpreter;

import com.smarthome.Main;
import com.smarthome.interpreter.variables.Iterable;
import com.smarthome.interpreter.variables.*;
import com.smarthome.interpreter.variables.Variable.VarType;
import com.smarthome.parser.Expression;
import com.smarthome.parser.Statement;
import com.smarthome.scanner.Token;
import com.smarthome.scanner.TokenType;

import java.util.*;
import java.util.function.BiFunction;


public class Resolver implements Expression.Visitor<Variable>, Statement.Visitor<Void> {
    private static final Map<TokenType, TriFunction<Token, Variable, Variable, Variable>> binaryExprTypeChecker = new HashMap<>();
    private static final Map<TokenType, BiFunction<Token, Variable, Variable>> unaryExprTypeChecker = new HashMap<>();

    static {
        binaryExprTypeChecker.put(TokenType.GREATER, (o, l, r) -> {
            checkNumericOperands(o, l, r);
            return new Variable(VarType.Bool, null);
        });
        binaryExprTypeChecker.put(TokenType.GREATER_EQUAL, (o, l, r) -> {
            checkNumericOperands(o, l, r);
            return new Variable(VarType.Bool, null);
        });
        binaryExprTypeChecker.put(TokenType.LESS, (o, l, r) -> {
            checkNumericOperands(o, l, r);
            return new Variable(VarType.Bool, null);
        });
        binaryExprTypeChecker.put(TokenType.LESS_EQUAL, (o, l, r) -> {
            checkNumericOperands(o, l, r);
            return new Variable(VarType.Bool, null);
        });
        binaryExprTypeChecker.put(TokenType.MINUS, (o, l, r) -> {
            checkNumericOperands(o, l, r);
            return new Variable(VarType.Bool, null);
        });
        binaryExprTypeChecker.put(TokenType.PLUS, (o, l, r) -> {
            if (!(l.varType == VarType.Number && r.varType == VarType.Number) &&
                    !(l.varType == VarType.String && r.varType == VarType.String)) {
                Main.error("Operands must be two numbers or two strings.", o.getLine(), o.getColumn());
            }
            return l;
        });
        binaryExprTypeChecker.put(TokenType.SLASH, (o, l, r) -> {
            checkNumericOperands(o, l, r);
            return new Variable(VarType.Bool, null);
        });
        binaryExprTypeChecker.put(TokenType.STAR, (o, l, r) -> {
            checkNumericOperands(o, l, r);
            return new Variable(VarType.Bool, null);
        });
        binaryExprTypeChecker.put(TokenType.EXCLAMATION_EQUAL, (o, l, r) -> {
            if (l.varType != r.varType) {
                Main.error("Comparable operands must be of the same type.", o.getLine(), o.getColumn());
            }
            return new Variable(VarType.Bool, null);
        });
        binaryExprTypeChecker.put(TokenType.EQUAL_EQUAL, (o, l, r) -> {
            if (l.varType != r.varType) {
                Main.error("Comparable operands must be of the same type.", o.getLine(), o.getColumn());
            }
            return new Variable(VarType.Bool, null);
        });
    }

    static {
        unaryExprTypeChecker.put(TokenType.NOT, (o, t) -> {
            if (t.varType != VarType.Bool) {
                Main.error("Operand must be a Bool.",
                        o.getLine(), o.getColumn());
            }
            return new Variable(VarType.Bool, null);
        });
        unaryExprTypeChecker.put(TokenType.MINUS, (o, t) -> {
            checkNumericOperand(o, t);
            return new Variable(VarType.Bool, null);
        });
    }

    private final Interpreter interpreter;
    private final Stack<Map<String, Variable>> scopes = new Stack<>();
    private Function currentFunction = null;
    private Place currentPlace = null;

    public Resolver(Interpreter interpreter) {
        beginScope();
        this.interpreter = interpreter;
    }

    private static void checkNumericOperand(Token operator, Variable operand) {
        if (operand.varType != VarType.Number) {
            Main.error("Operand must be a number.",
                    operator.getLine(), operator.getColumn());
        }
    }

    private static void checkNumericOperands(Token operator, Variable left, Variable right) {
        if (left.varType != VarType.Number || right.varType != VarType.Number) {
            Main.error("Operands must be numbers.",
                    operator.getLine(), operator.getColumn());
        }
    }

    public void resolve(List<Statement> statements) {
        for (Statement statement : statements) {
            if (statement != null) resolve(statement);
        }
    }

    @Override
    public Void visitBlockStmt(Statement.Block stmt) {
        beginScope();
        resolve(stmt.statements);
        endScope();
        return null;
    }

    @Override
    public Void visitPlaceStmt(Statement.Place stmt) {
        Place enclosingPlace = currentPlace;
        currentPlace = new Place(stmt.name.getLexeme(), null);

        declare(stmt.name, currentPlace);

        if (stmt.superPlace != null) {
            Variable superPlace = resolve(stmt.superPlace);

            if (!(superPlace instanceof Place)) {
                Main.error("SuperPlace must be a Place.",
                        stmt.name.getLine(), stmt.name.getColumn());
            } else {
                currentPlace.superPlace = (Place) superPlace;
            }
        }

        define(stmt.name);

        beginScope();
        resolve(stmt.block);
        for (Map.Entry<String, Variable> entry : scopes.peek().entrySet()) {
            currentPlace.set(entry.getKey(), entry.getValue());
        }
        endScope();

        currentPlace = enclosingPlace;
        return null;
    }

    @Override
    public Void visitThingStmt(Statement.Thing stmt) {
        declare(stmt.name, new Thing(stmt));
        Variable superThing = resolve(stmt.superInterface);

        if (!(superThing instanceof Interface)) {
            Main.error("SuperInterface must be an Interface.",
                    stmt.name.getLine(), stmt.name.getColumn());
        }

        resolve(stmt.call);
        define(stmt.name);
        return null;
    }

    @Override
    public Void visitExpressionStmt(Statement.Expression stmt) {
        resolve(stmt.expression);
        return null;
    }

    @Override
    public Void visitSimpleVarStmt(Statement.SimpleVar stmt) {
        Variable var = declare(stmt.name, new Variable(VarType.fromString(stmt.type.getLexeme()), null));
        if (stmt.initializer != null) {
            Variable value = resolve(stmt.initializer);
            if (value.varType != var.varType) {
                Main.error("Variable initializer is of different type than variable.",
                        stmt.name.getLine(), stmt.name.getColumn());
            }
        }

        define(stmt.name);
        return null;
    }

    @Override
    public Variable visitVariableExpr(Expression.Variable expr) {
        if (scopes.peek().containsKey(expr.name.getLexeme()) &&
                !scopes.peek().get(expr.name.getLexeme()).defined) {
            Main.error("Cannot read local variable in its own initializer.",
                    expr.name.getLine(), expr.name.getColumn());
        }

        return resolveVariable(expr, expr.name);
    }

    @Override
    public Variable visitAssignExpr(Expression.Assign expr) {
        Variable right = resolve(expr.value);
        Variable left = resolveVariable(expr, expr.name);

        if (left.varType != right.varType) {
            Main.error("Incompatible type of value.",
                    expr.name.getLine(), expr.name.getColumn());
        }

        return right;
    }

    @Override
    public Variable visitSetExpr(Expression.Set expr) {
        Variable right = resolve(expr.value);
        Variable gettable = resolve(expr.object);

        if (gettable instanceof Gettable) {
            Variable var = ((Gettable) gettable).get(expr.name.getLexeme());

            if (var == null) {
                Main.error("'" + ((Gettable) gettable).getName() + "' doesn't have '" +
                                expr.name.getLexeme() + "' property.",
                        expr.name.getLine(), expr.name.getColumn());

                return new Variable();
            }

            if (var.varType != right.varType) {
                Main.error("Incompatible type of value.",
                        expr.name.getLine(), expr.name.getColumn());
            }

            return var;
        } else {
            Main.error("This item is not gettable.",
                    expr.name.getLine(), expr.name.getColumn());
        }

        return right;
    }

    @Override
    public Variable visitBinaryExpr(Expression.Binary expr) {
        Variable left = resolve(expr.left);
        Variable right = resolve(expr.right);

        return binaryExprTypeChecker.get(expr.operator.getType()).apply(expr.operator, left, right);
    }

    @Override
    public Variable visitCallExpr(Expression.Call expr) {
        Variable callee = resolve(expr.callee);

        if (callee instanceof Interface) {
            return callee;
        }

        if (!(callee instanceof Callable)) {
            Main.error("This is not callable.", expr.paren.getLine(), expr.paren.getColumn());
            return new Variable();
        }

        Callable callable = (Callable) callee;

        if (expr.arguments.size() != callable.arity()) {
            Main.error("Expected " + callable.arity() + " arguments but got " +
                    expr.arguments.size() + ".", expr.paren.getLine(), expr.paren.getColumn());
        }

        for (int i = 0; i < expr.arguments.size(); i++) {
            Variable argType = resolve(expr.arguments.get(i));
            if (i < callable.arity()) {
                Variable paramType = callable.getParams().get(i);
                if (argType.varType != paramType.varType) {
                    Main.error("Incorrect type of argument number " + (i + 1) + ".",
                            expr.paren.getLine(), expr.paren.getColumn());
                }
            }
        }

        return callee;
    }

    @Override
    public Variable visitGetExpr(Expression.Get expr) {
        Variable gettable = resolve(expr.object);

        if (gettable instanceof Gettable) {
            Variable var = ((Gettable) gettable).get(expr.name.getLexeme());

            if (var == null) {
                Main.error("'" + ((Gettable) gettable).getName() + "' doesn't have '" +
                                expr.name.getLexeme() + "' property.",
                        expr.name.getLine(), expr.name.getColumn());

                return new Variable();
            }

            return var;
        } else {
            Main.error("This item is not gettable.",
                    expr.name.getLine(), expr.name.getColumn());
        }

        return new Variable();
    }

    @Override
    public Variable visitSubscriptionExpr(Expression.Subscription expr) {
        Main.error("Subscription is not supported yet.",
                expr.rightBracket.getLine(), expr.rightBracket.getColumn());
        return new Variable();
    }

    @Override
    public Variable visitPropertyRefExpr(Expression.PropertyRef expr) {
        Main.error("Property reference is not supported yet.",
                expr.name.getLine(), expr.name.getColumn());
        return new Variable();
    }

    @Override
    public Variable visitGroupingExpr(Expression.Grouping expr) {
        return resolve(expr.expression);
    }

    @Override
    public Variable visitLiteralExpr(Expression.Literal expr) {
        return new Variable(VarType.fromValue(expr.value), expr.value);
    }

    @Override
    public Variable visitLogicalExpr(Expression.Logical expr) {
        Variable left = resolve(expr.left);
        Variable right = resolve(expr.right);

        if (left.varType != VarType.Bool || right.varType != VarType.Bool) {
            Main.error("Logical expression must take two booleans.",
                    expr.operator.getLine(), expr.operator.getColumn());
        }

        return left;
    }

    @Override
    public Variable visitUnaryExpr(Expression.Unary expr) {
        Variable operand = resolve(expr.right);
        return unaryExprTypeChecker.get(expr.operator.getType()).apply(expr.operator, operand);
    }

    @Override
    public Void visitFunctionStmt(Statement.Function stmt) {
        Function function = new Function(stmt);
        declare(stmt.name, function);

        Function enclosingFunction = currentFunction;
        currentFunction = function;

        beginScope();
        for (Statement param : stmt.params) {
            resolve(param);
        }
        for (int i = 0; i < stmt.params.size(); i++) {
            function.addParam(((Variable) scopes.peek().values().toArray()[i]));
        }
        scopes.peek().put(stmt.name.getLexeme(), function);

        define(stmt.name);
        resolve(stmt.body);

        if (stmt.body.isEmpty()) {
            Main.error("Expect statement in a for body.",
                    stmt.name.getLine(), stmt.name.getColumn());
        } else {
            Statement lastStatement = stmt.body.get(stmt.body.size() - 1);
            if (!(lastStatement instanceof Statement.Return) && stmt.returns != null) {
                Main.error("Last statement in function must be a return statement.",
                        stmt.returns.getLine(), stmt.returns.getColumn());
            }
        }

        endScope();
        currentFunction = enclosingFunction;

        return null;
    }

    @Override
    public Void visitWhenStmt(Statement.When stmt) {
        Variable conditionType = resolve(stmt.expression);

        if (!stmt.changed) {
            if (conditionType.varType != VarType.Bool) {
                Main.error("When condition must be of Bool type.",
                        stmt.whenToken.getLine(), stmt.whenToken.getColumn());
            }
        } else {
            if (stmt.to != null) {
                Variable to = resolve(stmt.to);
                if (to.varType != conditionType.varType) {
                    Main.error("To value must be of the same type as condition expression.",
                            stmt.toToken.getLine(), stmt.toToken.getColumn());
                }
            }
            if (stmt.from != null) {
                Variable from = resolve(stmt.from);
                if (from.varType != conditionType.varType) {
                    Main.error("From value must be of the same type as condition expression.",
                            stmt.fromToken.getLine(), stmt.fromToken.getColumn());
                }
            }
        }

        beginScope();
        resolve(stmt.body);
        endScope();

        return null;
    }

    @Override
    public Void visitIfStmt(Statement.If stmt) {
        Variable conditionType = resolve(stmt.condition);
        if (conditionType.varType != VarType.Bool) {
            Main.error("Condition must be of Bool type.",
                    stmt.colon.getLine(), stmt.colon.getColumn());
        }

        if (((Statement.Block) stmt.ifBlock).statements.isEmpty()) {
            Main.error("Expect statement in a if body.",
                    stmt.colon.getLine(), stmt.colon.getColumn());
        }

        beginScope();
        resolve(stmt.ifBlock);
        endScope();

        if (stmt.elseBlock != null) {
            if (((Statement.Block) stmt.elseBlock).statements.isEmpty()) {
                Main.error("Expect statement in a else body.",
                        stmt.colon.getLine(), stmt.colon.getColumn());
            }

            beginScope();
            resolve(stmt.elseBlock);
            endScope();
        }

        return null;
    }

    @Override
    public Void visitReturnStmt(Statement.Return stmt) {
        if (currentFunction == null) {
            Main.error("You can only return from a function.",
                    stmt.keyword.getLine(), stmt.keyword.getColumn());
        }

        Variable returnValue = null;
        for (int i = scopes.size() - 1; i >= 0; i--) {
            if (scopes.get(i).containsKey(currentFunction.getName())) {
                returnValue = scopes.get(i).get(currentFunction.getName());
                break;
            }
        }

        if (stmt.value == null && ((Callable) returnValue).getReturnType() != VarType.NonType) {
            Main.error("Missing return value.",
                    stmt.keyword.getLine(), stmt.keyword.getColumn());
        } else if (stmt.value != null && ((Callable) returnValue).getReturnType() == VarType.NonType) {
            Main.error("Cannot return a value form a function with no return type declared.",
                    stmt.keyword.getLine(), stmt.keyword.getColumn());
        } else if (stmt.value != null) {
            Variable returnedType = resolve(stmt.value);

            if (returnedType.varType != ((Callable) returnValue).getReturnType()) {
                Main.error("Incorrect return type.",
                        stmt.keyword.getLine(), stmt.keyword.getColumn());
            }
        }

        return null;
    }

    private void resolve(Statement statement) {
        statement.accept(this);
    }

    private Variable resolve(Expression expression) {
        return expression.accept(this);
    }

    private void beginScope() {
        scopes.push(new LinkedHashMap<>());
    }

    private void endScope() {
        scopes.pop();
    }

    private Variable declare(Token name, Variable variable) {
        Map<String, Variable> scope = scopes.peek();
        if (scope.containsKey(name.getLexeme())) {
            Main.error("Item with this name already declared in this scope.",
                    name.getLine(), name.getColumn());
        }

        scope.put(name.getLexeme(), variable);
        return variable;
    }

    private void define(Token name) {
        Variable variable = scopes.peek().get(name.getLexeme());
        variable.defined = true;
        scopes.peek().put(name.getLexeme(), variable);
    }

    private Variable resolveVariable(Expression expression, Token name) {
        for (int i = scopes.size() - 1; i >= 0; i--) {
            if (scopes.get(i).containsKey(name.getLexeme())) {
                interpreter.resolve(expression, scopes.size() - i - 1);
                return scopes.get(i).get(name.getLexeme());
            }
        }

        Main.error("Cannot resolve symbol '" + name.getLexeme() + "'.",
                name.getLine(), name.getColumn());

        return new Variable();
    }

    @Override
    public Void visitPrintStmt(Statement.Print stmt) {
        Variable value = resolve(stmt.expression);
        if (value.varType == VarType.NonType) {
            Main.error("No value to print",
                    stmt.print.getLine(), stmt.print.getColumn());
        }

        return null;
    }

    @Override
    public Void visitForStmt(Statement.For stmt) {
        Variable in = resolve(stmt.in);

        if (!(in instanceof Iterable)) {
            Main.error("Item is not iterable.",
                    stmt.each.getLine(), stmt.each.getColumn());
        }

        beginScope();
        declare(stmt.each, in);
        define(stmt.each);

        if (((Statement.Block) stmt.body).statements.isEmpty()) {
            Main.error("Expect statement in a for body.",
                    stmt.each.getLine(), stmt.each.getColumn());
        }

        resolve(((Statement.Block) stmt.body).statements);
        endScope();

        return null;
    }

    @Override
    public Void visitImportStmt(Statement.Import stmt) {
        declare(stmt.category, new Interface(stmt.category.getLexeme()));
        if (!stmt.type.getLexeme().equals("interface")) {
            Main.error("Unrecognized importing type. Allowed are: 'interface'.",
                    stmt.type.getLine(), stmt.type.getColumn());
        }

        define(stmt.category);
        return null;
    }

    @FunctionalInterface
    interface TriFunction<T1, T2, T3, T4> {
        T4 apply(T1 one, T2 two, T3 three);
    }
}