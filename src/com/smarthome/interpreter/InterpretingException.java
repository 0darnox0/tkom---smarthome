package com.smarthome.interpreter;

import com.smarthome.scanner.Token;

public class InterpretingException extends RuntimeException {
    public final Token token;

    public InterpretingException(Token token, String message) {
        super(message);
        this.token = token;
    }
}
