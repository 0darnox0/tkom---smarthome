package com.smarthome.interpreter;

import com.smarthome.Main;
import com.smarthome.interpreter.variables.Iterable;
import com.smarthome.interpreter.variables.*;
import com.smarthome.parser.Expression;
import com.smarthome.parser.Statement;
import com.smarthome.scanner.Token;
import com.smarthome.scanner.TokenType;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;

public class Interpreter implements Expression.Visitor<Variable>, Statement.Visitor<Void> {
    private static final Map<TokenType, BiFunction<Object, Object, Object>> binaryExprEvaluator = new HashMap<>();
    private static final Map<TokenType, java.util.function.Function<Object, Object>> unaryExprEvaluator = new HashMap<>();

    static {
        binaryExprEvaluator.put(TokenType.GREATER, (l, r) -> (double) l > (double) r);
        binaryExprEvaluator.put(TokenType.GREATER_EQUAL, (l, r) -> (double) l >= (double) r);
        binaryExprEvaluator.put(TokenType.LESS, (l, r) -> (double) l < (double) r);
        binaryExprEvaluator.put(TokenType.LESS_EQUAL, (l, r) -> (double) l <= (double) r);
        binaryExprEvaluator.put(TokenType.EXCLAMATION_EQUAL, (l, r) -> !isEqual(l, r));
        binaryExprEvaluator.put(TokenType.EQUAL_EQUAL, (l, r) -> isEqual(l, r));
        binaryExprEvaluator.put(TokenType.MINUS, (l, r) -> (double) l - (double) r);
        binaryExprEvaluator.put(TokenType.PLUS, (l, r) -> {
            if (l instanceof Double) {
                return (double) l + (double) r;
            }
            if (l instanceof String) {
                return (String) l + r;
            }
            throw new InterpretingException(new Token(TokenType.PLUS, "", null, 0, 0),
                    "Addition not allowed.");
        });
        binaryExprEvaluator.put(TokenType.SLASH, (l, r) -> (double) l / (double) r);
        binaryExprEvaluator.put(TokenType.STAR, (l, r) -> (double) l * (double) r);
    }

    static {
        unaryExprEvaluator.put(TokenType.NOT, (o) -> !isTrue(o));
        unaryExprEvaluator.put(TokenType.MINUS, (o) -> -(double) o);
    }

    private final Environment globals = new Environment();
    private final Map<Expression, Integer> locals = new HashMap<>();
    private final List<When> whenActions = new LinkedList<>();
    private Environment environment = globals;

    public static boolean isTrue(Object object) {
        return (boolean) object;
    }

    private static boolean isEqual(Object a, Object b) {
        return a.equals(b);
    }

    public void interpret(List<Statement> statements) {
        try {
            for (Statement statement : statements) {
                if (statement != null) execute(statement);
            }
        } catch (InterpretingException error) {
            Main.interpretingError(error);
        }

    }

    @Override
    public Variable visitLiteralExpr(Expression.Literal expr) {
        return new Variable(expr.value);
    }

    public void resolve(Expression expression, int depth) {
        locals.put(expression, depth);
    }

    @Override
    public Variable visitLogicalExpr(Expression.Logical expr) {
        Variable left = evaluate(expr.left);

        if (expr.operator.getType() == TokenType.OR) {
            if (isTrue(left.value)) return left;
        } else {
            if (!isTrue(left.value)) return left;
        }

        return evaluate(expr.right);
    }

    @Override
    public Void visitBlockStmt(Statement.Block stmt) {
        executeBlock(stmt.statements, new Environment(environment));
        return null;
    }

    @Override
    public Variable visitGroupingExpr(Expression.Grouping expr) {
        return evaluate(expr.expression);
    }

    public Variable evaluate(Expression expression) {
        return expression.accept(this);
    }

    @Override
    public Void visitExpressionStmt(Statement.Expression stmt) {
        evaluate(stmt.expression);
        return null;
    }

    void execute(Statement statement) {
        statement.accept(this);
    }

    public void executeBlock(List<Statement> statements, Environment environment) {
        Environment previous = this.environment;
        try {
            this.environment = environment;

            for (Statement statement : statements) {
                execute(statement);
            }
        } finally {
            this.environment = previous;
        }
    }

    @Override
    public Void visitWhenStmt(Statement.When stmt) {
        Object fromValue = null;
        Object toValue = null;

        if (stmt.from != null) fromValue = evaluate(stmt.from).value;
        if (stmt.to != null) toValue = evaluate(stmt.to).value;

        When when = new When(stmt, environment, fromValue, toValue);
        whenActions.add(when);
        return null;
    }

    @Override
    public Void visitIfStmt(Statement.If stmt) {
        if (isTrue(evaluate(stmt.condition).value)) {
            execute(stmt.ifBlock);
        } else if (stmt.elseBlock != null) {
            execute(stmt.elseBlock);
        }
        return null;
    }

    @Override
    public Void visitReturnStmt(Statement.Return stmt) {
        Variable value = null;
        if (stmt.value != null) value = evaluate(stmt.value);

        throw new Return(value);
    }

    @Override
    public Void visitSimpleVarStmt(Statement.SimpleVar stmt) {
        Variable value = null;
        if (stmt.initializer != null) {
            value = evaluate(stmt.initializer);
        }

        environment.define(stmt.name.getLexeme(), value);
        return null;
    }

    @Override
    public Void visitForStmt(Statement.For stmt) {
        Variable in = evaluate(stmt.in);

        for (Variable element : ((Iterable) in).getCollection().getElements()) {
            environment = new Environment(environment);

            environment.define(stmt.each.getLexeme(), element);
            executeBlock(((Statement.Block) stmt.body).statements, environment);
            environment = environment.enclosing;
        }

        return null;
    }

    @Override
    public Void visitImportStmt(Statement.Import stmt) {
        Interface inter = new Interface(stmt.category.getLexeme());
        environment.define(stmt.category.getLexeme(), inter);

        return null;
    }

    @Override
    public Variable visitAssignExpr(Expression.Assign expr) {
        Variable value = evaluate(expr.value);
        environment.assign(expr.name, value);

        return value;
    }

    @Override
    public Variable visitBinaryExpr(Expression.Binary expr) {
        Variable left = evaluate(expr.left);
        Variable right = evaluate(expr.right);

        Object result = binaryExprEvaluator.get(expr.operator.getType()).apply(left.value, right.value);
        return new Variable(result);
    }

    @Override
    public Variable visitCallExpr(Expression.Call expr) {
        Callable callee = (Callable) evaluate(expr.callee);

        List<Variable> arguments = new ArrayList<>();
        for (Expression argument : expr.arguments) {
            arguments.add(evaluate(argument));
        }

        return callee.call(this, arguments);
    }

    @Override
    public Void visitFunctionStmt(Statement.Function stmt) {
        Function function = new Function(stmt, environment);
        environment.define(stmt.name.getLexeme(), function);
        return null;
    }

    @Override
    public Variable visitSubscriptionExpr(Expression.Subscription expr) {
        throw new NotImplementedException();
    }

    @Override
    public Variable visitPropertyRefExpr(Expression.PropertyRef expr) {
        throw new NotImplementedException();
    }

    @Override
    public Variable visitUnaryExpr(Expression.Unary expr) {
        Variable operand = evaluate(expr.right);

        Object result = unaryExprEvaluator.get(expr.operator.getType()).apply(operand.value);
        return new Variable(result);
    }

    @Override
    public Variable visitVariableExpr(Expression.Variable expr) {
        return lookUpVariable(expr.name, expr);
    }

    @Override
    public Void visitPrintStmt(Statement.Print stmt) {
        Variable variable = evaluate(stmt.expression);
        System.out.println(variable.value);
        return null;
    }

    private Variable lookUpVariable(Token name, Expression expression) {
        Integer distance = locals.get(expression);
        if (distance != null) {
            return environment.getAt(distance, name.getLexeme());
        } else {
            return globals.get(name);
        }
    }

    @Override
    public Void visitPlaceStmt(Statement.Place stmt) {
        Object superPlace = null;
        if (stmt.superPlace != null) {
            superPlace = evaluate(stmt.superPlace);
        }

        environment.define(stmt.name.getLexeme(), null);
        Environment env = new Environment(environment);
        executeBlock(stmt.block, env);

        Place place = new Place(stmt.name.getLexeme(), (Place) superPlace);
        place.properties = env.values;

        if (superPlace != null) {
            ((Place) superPlace).addChildren(place);
        }

        environment.assign(stmt.name, place);
        return null;
    }

    @Override
    public Void visitThingStmt(Statement.Thing stmt) {
        Interface superInterface = (Interface) evaluate(stmt.superInterface);
        environment.define(stmt.name.getLexeme(), null);

        Thing thing = new Thing(stmt, superInterface);
        superInterface.addChildren(thing);

        environment.assign(stmt.name, thing);
        return null;
    }

    @Override
    public Variable visitSetExpr(Expression.Set expr) {
        Variable value = evaluate(expr.value);
        Gettable object = (Gettable) evaluate(expr.object);

        object.set(expr.name.getLexeme(), value);
        return value;
    }

    @Override
    public Variable visitGetExpr(Expression.Get expr) {
        Gettable gettable = (Gettable) evaluate(expr.object);
        return gettable.get(expr.name.getLexeme());
    }

    public Thread runThread() {
        return new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                for (When action : whenActions) {
                    action.call(this);
                }
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
