package com.smarthome.interpreter;

import com.smarthome.interpreter.variables.Variable;
import com.smarthome.scanner.Token;

import java.util.HashMap;
import java.util.Map;

public class Environment {
    final Environment enclosing;
    final Map<String, Variable> values = new HashMap<>();


    Environment() {
        enclosing = null;
    }

    public Environment(Environment enclosing) {
        this.enclosing = enclosing;
    }

    Variable get(Token name) {
        if (values.containsKey(name.getLexeme())) {
            return values.get(name.getLexeme());
        }

        if (enclosing != null) return enclosing.get(name);

        throw new InterpretingException(name,
                "Undefined variable '" + name.getLexeme() + "'.");
    }

    public void assign(Token name, Variable value) {
        if (values.containsKey(name.getLexeme())) {
            values.put(name.getLexeme(), value);
            return;
        }

        if (enclosing != null) {
            enclosing.assign(name, value);
            return;
        }

        throw new InterpretingException(name,
                "Undefined variable '" + name.getLexeme() + "'.");
    }

    public void define(String name, Variable value) {
        values.put(name, value);
    }

    private Environment ancestor(int distance) {
        Environment environment = this;
        for (int i = 0; i < distance; i++) {
            environment = environment.enclosing;
        }

        return environment;
    }

    public Variable getAt(int distance, String name) {
        return ancestor(distance).values.get(name);
    }

    void assignAt(int distance, Token name, Variable value) {
        ancestor(distance).values.put(name.getLexeme(), value);
    }

}
