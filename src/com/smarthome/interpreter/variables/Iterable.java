package com.smarthome.interpreter.variables;

public interface Iterable {
    Collection getCollection();
}
