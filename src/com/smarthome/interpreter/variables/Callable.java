package com.smarthome.interpreter.variables;

import com.smarthome.interpreter.Interpreter;

import java.util.List;

public interface Callable {
    int arity();

    Variable call(Interpreter interpreter, List<Variable> arguments);

    String getName();

    List<Variable> getParams();

    Variable.VarType getReturnType();

    void addParam(Variable param);
}
