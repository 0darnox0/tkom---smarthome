package com.smarthome.interpreter.variables;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Place extends Variable implements Gettable {
    final String name;
    final List<Place> children;
    public Map<String, Variable> properties;
    public Place superPlace;

    public Place(String name, Place superPlace) {
        super.varType = VarType.Place;
        super.value = this;
        this.superPlace = superPlace;
        this.name = name;
        this.properties = new HashMap<>();
        children = new LinkedList<>();
    }

    public void addChildren(Place children) {
        this.children.add(children);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public Variable get(String name) {
        if (properties.containsKey(name)) {
            return properties.get(name);
        }

        if (superPlace != null) {
            return superPlace.get(name);
        }

        return null;
    }

    @Override
    public void set(String name, Variable value) {
        properties.put(name, value);
    }

    @Override
    public String getName() {
        return name;
    }
}
