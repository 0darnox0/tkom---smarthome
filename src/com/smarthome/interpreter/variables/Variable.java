package com.smarthome.interpreter.variables;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Variable {
    public VarType varType;
    public Object value;
    public boolean defined = false;


    public Variable(Object value) {
        if (value instanceof Boolean) {
            this.value = value;
            varType = VarType.Bool;
        } else if (value instanceof Double) {
            this.value = value;
            varType = VarType.Number;
        } else if (value instanceof String) {
            this.value = value;
            varType = VarType.String;
        }
    }

    public Variable(double value) {
        this.value = value;
        varType = VarType.Number;
    }

    public Variable(String value) {
        this.value = value;
        varType = VarType.String;
    }

    public Variable(boolean value) {
        this.value = value;
        varType = VarType.Bool;
    }

    public Variable() {
        this(VarType.NonType, null);
    }

    public Variable(VarType varType, Object value) {
        this.varType = varType;
        this.value = value;
    }

    public enum VarType {
        Number("Number"),
        String("String"),
        Bool("Bool"),
        Place("Place"),
        Thing("Thing"),
        Property("Property"),
        Interface("interface"),
        NonType("NonType"),
        Function("Function");

        private static final Map<String, VarType> string2VarType;

        static {
            Map<String, VarType> map = new HashMap<>();
            for (VarType instance : VarType.values()) {
                map.put(instance.toString(), instance);
            }
            string2VarType = Collections.unmodifiableMap(map);
        }

        private String name;

        VarType(String name) {
            this.name = name;
        }

        public static VarType fromString(String name) {
            return string2VarType.get(name);
        }

        public static VarType fromValue(Object value) {
            if (value instanceof Boolean) return Bool;
            if (value instanceof Double) return Number;
            if (value instanceof String) return String;
            throw new NotImplementedException();
        }

        @Override
        public String toString() {
            return this.name;
        }
    }
}

