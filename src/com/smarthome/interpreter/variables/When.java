package com.smarthome.interpreter.variables;

import com.smarthome.interpreter.Environment;
import com.smarthome.interpreter.Interpreter;
import com.smarthome.parser.Statement;

public class When {
    private final Statement.When declaration;
    private final Environment closure;
    private final boolean expectChange;
    private final Object expectFrom;
    private final Object expectTo;
    private Object previousValue = null;

    public When(Statement.When declaration, Environment closure, Object from, Object to) {
        this.closure = closure;
        this.declaration = declaration;
        expectFrom = from;
        expectTo = to;
        expectChange = declaration.changed;
    }

    public void call(Interpreter interpreter) {
        Environment environment = new Environment(closure);

        Variable value = interpreter.evaluate(declaration.expression);

        if (!expectChange) {
            interpreter.executeBlock(declaration.body, environment);
        } else {
            if (expectFrom != null && !expectFrom.equals(previousValue)) return;
            if (expectTo != null && !expectTo.equals(value.value)) return;

            previousValue = value;
            interpreter.executeBlock(declaration.body, environment);
        }
    }
}
