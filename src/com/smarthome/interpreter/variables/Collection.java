package com.smarthome.interpreter.variables;

import java.util.LinkedList;
import java.util.List;

public class Collection extends Variable implements Iterable {
    private final List<Variable> elements;

    Collection() {
        elements = new LinkedList<>();
    }

    Collection(List<Variable> elements) {
        this.elements = elements;
    }

    Collection(List<Variable> elements, VarType type) {
        this.elements = elements;
        this.varType = type;
    }

    public boolean isEmpty() {
        return elements.isEmpty();
    }

    public List<Variable> getElements() {
        return elements;
    }

    public void addElement(Variable element) {
        elements.add(element);
    }

    @Override
    public Collection getCollection() {
        return this;
    }
}
