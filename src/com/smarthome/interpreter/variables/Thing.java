package com.smarthome.interpreter.variables;

import com.smarthome.parser.Statement;

public class Thing extends Variable implements Gettable {
    private final Statement.Thing declaration;
    private Interface superInterface;

    public Thing(Statement.Thing declaration) {
        super.varType = VarType.Thing;
        super.value = this;
        this.declaration = declaration;
    }

    public Thing(Statement.Thing declaration, Interface superInterface) {
        super.varType = VarType.Thing;
        super.value = this;
        this.declaration = declaration;
        this.superInterface = superInterface;
    }

    @Override
    public Variable get(String name) {
        // This is a hack to allow thing be gettable in any way
        return new Function();
    }

    @Override
    public void set(String name, Variable value) {

    }

    @Override
    public String getName() {
        return declaration.name.getLexeme();
    }

    @Override
    public String toString() {
        return getName();
    }
}
