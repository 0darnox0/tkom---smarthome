package com.smarthome.interpreter.variables;

import com.smarthome.interpreter.Interpreter;

import java.util.LinkedList;
import java.util.List;

public class Interface extends Variable implements Callable, Gettable, Iterable {
    final String name;
    final List<Thing> children;
    private final List<Variable> params;

    public Interface(String name) {
        super.varType = VarType.Interface;
        super.value = this;
        this.name = name;
        children = new LinkedList<>();
        params = new LinkedList<>();
    }

    public void addChildren(Thing children) {
        this.children.add(children);
    }

    @Override
    public int arity() {
        return 0;
    }

    @Override
    public Variable call(Interpreter interpreter, List<Variable> arguments) {
        return null;
    }

    @Override
    public Variable get(String name) {
        Collection collection = new Collection((List) children);
        collection.varType = VarType.Thing;
        return collection;
    }

    @Override
    public void set(String name, Variable value) {

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Variable> getParams() {
        return params;
    }

    @Override
    public VarType getReturnType() {
        return VarType.Interface;
    }

    @Override
    public void addParam(Variable param) {
        params.add(param);
    }

    @Override
    public Collection getCollection() {
        return new Collection((List) children, VarType.Thing);
    }
}
